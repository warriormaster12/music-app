#ifndef EQUALIZER_H
#define EQUALIZER_H


#include <QtCore/QObject>
#include <pulse/pulseaudio.h> 



class Equalizer : public QObject
{
    Q_PROPERTY(QString test_text READ test_text WRITE set_test_text NOTIFY test_text_changed)

signals:
    void test_text_changed();
private: 
    QString test_text();
    void set_test_text(QString value);
};

#endif //EQUALIZER_H