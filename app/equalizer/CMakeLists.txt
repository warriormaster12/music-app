# QML plugin
project(equalizer_plugin)

set(plugin_SRCS
    equalizer.cpp
    )

set(plugin_HDRS
    equalizer.h
    )

add_library(equalizer-qml SHARED ${plugin_SRCS} ${plugin_HDRS})
file(COPY qmldir DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
target_link_libraries(equalizer-qml
    ${Qt5Qml_LIBRARIES}
    pulse
    )

install(TARGETS equalizer-qml DESTINATION ${PLUGIN_DIR})
install(FILES qmldir DESTINATION ${PLUGIN_DIR})
