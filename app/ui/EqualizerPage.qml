/*
 * Copyright: 2020 UBports
 *
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "../components"
import Equalizer 0.1

Page {
    id: equalizerPage
    header: PageHeader {
        title: i18n.tr("Equalizer")
        leadingActionBar {
            actions: backActionComponent
        }
        StyleHints {
            backgroundColor: mainView.headerColor
            dividerColor: Qt.darker(mainView.headerColor, 1.1)
        }
        Action {
            id: backActionComponent
            iconName: "back"
            objectName: "backAction"
            onTriggered: mainPageStack.pop()
        }
    }

    Flickable {
        id: equalizerFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded
        anchors {
            topMargin: equalizerPage.header.height + units.gu(1)
            fill: parent
        }
        contentHeight: equalizerColumn.childrenRect.height
        Column {
            id: equalizerColumn
            anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
            }

            ListItem {
                id: equalizerEnableItem
                height: equalizerEnableLayout.height
                divider.visible: false
                ListItemLayout {
                    id: equalizerEnableLayout
                    title.text: i18n.tr("Enable")
                    Switch {
                        id: displayOnSwitch
                        SlotsLayout.position: SlotsLayout.Last
                    }
                }
            }
            ListItem {
                id: displayOnItem2
                height: displayOnLayout1.height
                divider.visible: false
                Row {
                    id: equalizerRow
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: displayOnLayout1
                        title.text: i18n.tr("60 Hz")
                        Slider {
                            id: equalizerSlider
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: -10
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            }
            ListItem {
                id: displayOnItem3
                height: displayOnLayout2.height
                divider.visible: false
                Row {
                    id: equalizerRow2
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: displayOnLayout2
                        title.text: i18n.tr("230 Hz")
                        Slider {
                            id: equalizerSlider2
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: -10
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            }
            ListItem {
                id: displayOnItem4
                height: displayOnLayout3.height
                divider.visible: false
                Row {
                    id: equalizerRow3
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: displayOnLayout3
                        title.text: i18n.tr("910 Hz")
                        Slider {
                            id: equalizerSlider3
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: -10
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            }
            ListItem {
                id: displayOnItem5
                height: displayOnLayout4.height
                divider.visible: false
                Row {
                    id: equalizerRow4
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: displayOnLayout4
                        title.text: i18n.tr("4 kHz")
                        Slider {
                            id: equalizerSlider4
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: -10
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            }
            ListItem {
                id: displayOnItem6
                height: displayOnLayout5.height
                divider.visible: false
                Row {
                    id: equalizerRow5
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: displayOnLayout5
                        title.text: i18n.tr("14 kHz")
                        Slider {
                            id: equalizerSlider5
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: -10
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            }
            ListItem {
                id: bassBoostItem
                height: bassBoostLayout.height
                divider.visible: false
                Row {
                    id: bassBoostRow
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    ListItemLayout {
                        id: bassBoostLayout
                        title.text: i18n.tr("Bass boost")
                        Slider {
                            id: bassBoostSlider
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: 0
                            maximumValue: 10
                            value: 0.0
                            live: true
                        }
                    }
                    
                }
            
            }
            
        }
        
    }
}
